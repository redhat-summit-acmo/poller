from flask_restplus import Api
from view.poller_view import api as poller_api


api = Api(
    title='Poller',
    version='0.1',
    description='Poller',
    doc='/apidoc/'
)

api.add_namespace(poller_api, path='/poller')
