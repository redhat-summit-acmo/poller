from flask import jsonify, request
from flask_restplus import Namespace, Resource
from service.poller_service import PollerService

api = Namespace('application', description='Application API')


@api.route("")
class PollerView(Resource):

    def __init__(self, api):
        Resource.__init__(self, api)
        self._poller_service = PollerService()

    def get(self):
        return jsonify({"response": self._poller_service.ping_device()})
